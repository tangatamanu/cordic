# CORDIC

VHDL implementation of a CORDIC sinewave generator [v0.1]

# Structure

The generator is fully pipelined. An implementation of a single stage of the pipeline can be found in [CORDIC_STAGE.vhd](src/CORDIC_STAGE.vhd). [CORDIC_PIPELINE.vhd](src/CORDIC_PIPELINE.vhd) serves as the main structure, including a table of arctan(2^-i) values for up to 18 stages. [CORDIC_SYNC.vhd](src/CORDIC_SYNC.vhd) generates a triangular wave and feeds the algorithm, as well as synchronizes the cosine output (as multiplying by -1 every half-period of the triangle is neccessary).
