library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity CORDIC_SYNC is
  
  generic (
    DEPTH   : integer := 16;            -- Bitwidth (misnomer)
    NUMITER : integer := 12;  -- Number of CORDIC iterations (in general, DEPTH-4 is maximum)
    step    : integer := 40);  -- step length for the generated wave,  in general (2**DEPTH-1)/step gives about wave frequency

  port (
    I_CLK   : in  std_logic;            -- Incoming Clock
    I_RST_N : in  std_logic;            -- Active-low reset
    COS_O   : out signed(DEPTH-1 downto 0);
    SIN_O   : out signed(DEPTH-1 downto 0)
    );

end entity CORDIC_SYNC;

architecture rtl of CORDIC_SYNC is
  
  component CORDIC_PIPELINE is
    generic (
      DEPTH   : integer;
      NUMITER : integer);
    port (
      I_CLK   : in  std_logic;
      I_RESET : in  std_logic;
      Z_IN    : in  std_logic_vector(DEPTH-1 downto 0);
      X_OUT   : out std_logic_vector(DEPTH-1 downto 0);
      Y_OUT   : out std_logic_vector(DEPTH-1 downto 0);
      Z_DEL   : out std_logic_vector(DEPTH-1 downto 0));
  end component CORDIC_PIPELINE;
 
  signal Z_TRI    : signed(DEPTH-1 downto 0) := (others => '0');
  signal Z_DEL   : std_logic_vector(DEPTH-1 downto 0) ;
  signal Z_DEL_SIG : signed(DEPTH-1 downto 0);
  signal sqx : std_logic := '0';
  signal pmtriflags : std_logic_vector(1 downto 0) := "00";
  signal X_HALF : std_logic_vector(DEPTH-1 downto 0):= (others => '0');
  signal sine : std_logic_vector(DEPTH-1 downto 0) := (others => '0');
begin  -- architecture rtl

Z_DEL_SIG <= signed(Z_DEL);
SIN_O <= signed(sine);
COS_O <= signed(X_HALF) when sqx = '0' else -signed(X_HALF);
  
  CORDIC_PIPELINE_1: CORDIC_PIPELINE
    generic map (
      DEPTH   => DEPTH,
      NUMITER => NUMITER)
    port map (
      I_CLK   => I_CLK,
      I_RESET => I_RST_N,
      Z_IN    => std_logic_vector(Z_TRI),
      X_OUT   => X_HALF,
      Y_OUT   => sine,
      Z_DEL   => Z_DEL);
  
 -- purpose: Generate a triangle wave
 -- type   : sequential
 -- inputs : I_CLK, I_RST_N
 -- outputs: 
TRIWAVE: process (I_CLK, I_RST_N) is
 begin  -- process TRIWAVE
   if I_RST_N = '0' then                -- asynchronous reset (active low)
     Z_TRI <= (others => '0');
     sqx <= '0';
   elsif I_CLK'event and I_CLK = '1' then  -- rising clock edge
     if Z_DEL_SIG = (2**(DEPTH-2) - (2**(DEPTH-2) mod step)) + step*2 then
       sqx <= not sqx;
     elsif  Z_DEL_SIG = -((2**(DEPTH-2) - (2**(DEPTH-2) mod step)) + step*2) then 
       sqx <= not sqx;    
     end if;
     case pmtriflags is 
        when "00" => 
            Z_TRI <= Z_TRI + step;
            if Z_TRI(DEPTH-1 downto DEPTH-2) = "01" then 
                pmtriflags <= "01";
            else 
                pmtriflags <= "00";
            end if;     
        when "01" => 
            Z_TRI <= Z_TRI - step;
            if Z_TRI(DEPTH-1 downto DEPTH-2) = "10" then 
              pmtriflags <= "00";
            else
                pmtriflags <= "01";
            end if;
        when others => 
            null;
    end case; 
   end if;
 end process TRIWAVE; 



  
end architecture rtl;
