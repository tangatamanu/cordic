library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.math_real.all;

entity CORDIC_STAGE is
  generic (
    ITER : integer := 0;
    DEPTH : integer := 16
      
  );
  port (
    CLK   : in    std_logic;
    Y_IN  : in    std_logic_vector(DEPTH-1 downto 0);
    X_IN  : in    std_logic_vector(DEPTH-1 downto 0);
    Z_IN  : in    std_logic_vector(DEPTH-1 downto 0);
    Z_OUT : out   std_logic_vector(DEPTH-1 downto 0);
    X_OUT : out   std_logic_vector(DEPTH-1 downto 0);
    Y_OUT : out   std_logic_vector(DEPTH-1 downto 0);
    Z_DEL_IN : in std_logic_vector(DEPTH-1 downto 0);
    Z_DEL_OUT: out std_logic_vector(DEPTH-1 downto 0);
    
    TANFI : in    signed(21 downto 0)
  );
end entity CORDIC_STAGE;

architecture RTL of CORDIC_STAGE is

  signal xdiv   : signed(DEPTH-1 downto 0) := (others => '0');
  signal ydiv   : signed(DEPTH-1 downto 0) := (others => '0');
  signal dir    : std_logic := '0';
  signal z_sign : signed(DEPTH-1 downto 0) := (others => '0');
  signal tanact : signed(DEPTH-1 downto 0) := (others => '0');
begin

  xdiv <= shift_right(signed(X_IN), ITER);
  ydiv <= shift_right(signed(Y_IN), ITER);
  dir  <= z_in(DEPTH-1);

  tanact <= signed(TANFI(21 downto (22-DEPTH)));
  
  
  ROTATE : process (clk) is

  begin

    if rising_edge(clk) then
      Z_DEL_OUT <= Z_DEL_IN;
      if (dir='1') then
        X_OUT <= std_logic_vector(signed(X_IN) + ydiv);
        Y_OUT <= std_logic_vector(signed(Y_IN) - xdiv);
        Z_OUT <= std_logic_vector(signed(Z_IN) + tanact);
      else
        X_OUT <= std_logic_vector(signed(X_IN) - ydiv);
        Y_OUT <= std_logic_vector(signed(Y_IN) + xdiv);
        Z_OUT <= std_logic_vector(signed(Z_IN) - tanact);
      end if;
    end if;

  end process ROTATE;

end architecture RTL;

