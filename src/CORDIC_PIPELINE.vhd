library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity CORDIC_PIPELINE is
  generic (
    DEPTH   : integer := 22;
    NUMITER : integer := 18
  );
  port (
    I_CLK   : in  std_logic;
    I_RESET : in  std_logic;
    Z_IN    : in  std_logic_vector(DEPTH-1 downto 0);
    X_OUT   : out std_logic_vector(DEPTH-1 downto 0);
    Y_OUT   : out std_logic_vector(DEPTH-1 downto 0);
    Z_DEL   : out std_logic_vector(DEPTH-1 downto 0)
  );
end entity CORDIC_PIPELINE;

architecture rtl of CORDIC_PIPELINE is

  component CORDIC_STAGE
    generic (
      ITER : integer;
      DEPTH : integer
    );
      port (
      CLK : in std_logic;
      Y_IN : in std_logic_vector(DEPTH-1 downto 0);
      X_IN : in std_logic_vector(DEPTH-1 downto 0);
      Z_IN : in std_logic_vector(DEPTH-1 downto 0);
      Z_OUT : out std_logic_vector(DEPTH-1 downto 0);
      X_OUT : out std_logic_vector(DEPTH-1 downto 0);
      Y_OUT : out std_logic_vector(DEPTH-1 downto 0);
      Z_DEL_IN : in std_logic_vector(DEPTH-1 downto 0);
      Z_DEL_OUT: out std_logic_vector(DEPTH-1 downto 0);
      TANFI : in signed(21 downto 0)
    );
  end component;
  

  type angle22 is array (0 to 18) of signed(21 downto 0);
    constant CORDIC_ANGLES : angle22 :=
    (
"0010000000000000000000", --080000
"0001001011100100000001", --04b901
"0000100111111011001110", --027ece
"0000010100010001000100", --014444
"0000001010001011000011", --00a2c3
"0000000101000101110101", --005175
"0000000010100010111101", --0028bd
"0000000001010001011111", --00145f
"0000000000101000101111", --000a2f
"0000000000010100010111", --000517
"0000000000001010001011", --00028b
"0000000000000101000101", --000145
"0000000000000010100010", --0000a2
"0000000000000001010001", --000051
"0000000000000000101000", --000028
"0000000000000000010100", --000014
"0000000000000000001010", --00000a
"0000000000000000000101", --000005
"0000000000000000000010"  --000002

);
  
  
  type regiter is array (0 to NUMITER-1) of std_logic_vector(DEPTH-1 downto 0);
  signal X : regiter := (others => (others => '0'));
  signal Y : regiter:= (others => (others => '0'));
  signal Z : regiter:= (others => (others => '0'));
  signal del : regiter:= (others => (others => '0'));
  constant xin : std_logic_vector(DEPTH-1 downto 0) := "0010101000001100001111";
begin
  UNROLLED : for k in 0 to NUMITER-1
       generate
      STAGE1:  if k = 0 generate
       ST1 : CORDIC_STAGE
       generic map (ITER => k, DEPTH => DEPTH)
        port map
        ( CLK => I_CLK,
          Y_IN => (others => '0'),
          X_IN => xin,--set the scaling factor?
          Z_IN => Z_IN,
          X_OUT => X(k),
          Y_OUT => Y(k),
          Z_OUT => Z(k),
          Z_DEL_IN => Z_IN,
          Z_DEL_OUT => del(k),
          TANFI => CORDIC_ANGLES(k)
        );
      end generate STAGE1;
      STAGEN : if (k > 0) generate
        STN : CORDIC_STAGE
        generic map (
          ITER => k, DEPTH => DEPTH)
                  port map
          (
            CLK => I_CLK,
            Y_IN => Y(k-1),
            X_IN => X(k-1),
            Z_IN => Z(k-1),
            X_OUT => X(k),
            Y_OUT => Y(k),
            Z_DEL_IN => del(k-1),
            Z_DEL_OUT => del(k),
            Z_OUT => Z(k),
            TANFI => CORDIC_ANGLES(k)
          );
      end generate STAGEN;
    end generate UNROLLED;
X_OUT <= X(NUMITER-1);
Y_OUT <= Y(NUMITER-1);
Z_DEL <= del(NUMITER-1);
end architecture;

    
