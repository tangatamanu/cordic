library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CORDIC_PIPELINE_tb is
end;

architecture bench of CORDIC_PIPELINE_tb is

  component CORDIC_PIPELINE
    generic (
      DEPTH : integer;
      NUMITER : integer
    );
      port (
      I_CLK : in std_logic;
      I_RESET : in std_logic;
      Z_IN : in std_logic_vector(DEPTH-1 downto 0);
      X_OUT : out std_logic_vector(DEPTH-1 downto 0);
      Y_OUT : out std_logic_vector(DEPTH-1 downto 0)
    );
  end component;

  -- Clock period
  constant clk_period : time := 10 ns;
  -- Generics
  constant DEPTH : integer := 16;
  constant NUMITER : integer := 12;

  -- Ports
  signal I_CLK : std_logic;
  signal I_RESET : std_logic;
  signal Z_IN : std_logic_vector(DEPTH-1 downto 0) := (others => '0');
  signal X_OUT : std_logic_vector(DEPTH-1 downto 0);
  signal Y_OUT : std_logic_vector(DEPTH-1 downto 0);
  signal XTEST : std_logic_vector(DEPTH downto 0);
    signal trigflag : std_logic := '0';
    signal sigtest : signed(DEPTH-1 downto 0);
    signal flag2 : std_logic := '0';
begin

  CORDIC_PIPELINE_inst : CORDIC_PIPELINE
    generic map (
      DEPTH => DEPTH,
      NUMITER => NUMITER
    )
    port map (
      I_CLK => I_CLK,
      I_RESET => I_RESET,
      Z_IN => Z_IN,
      X_OUT => X_OUT,
      Y_OUT => Y_OUT
    );

  clk_process : process
  begin
  I_CLK <= '1';
  wait for clk_period/2;
  I_CLK <= '0';
  wait for clk_period/2;
  end process clk_process;

 dut: process(I_CLK)
  begin
    if rising_edge(I_CLK) then
      I_RESET <= '0';
      if flag2 = '0' then 
      sigtest <= signed(x_out); 
      if trigflag = '0' then
      Z_IN <= std_logic_vector(signed(Z_IN) +100);
        if Z_IN >= x"2200" then 
            trigflag <= '1';
            flag2 <= '1';

        end if;     
      else 
      Z_IN <= std_logic_vector(signed(Z_IN) -100);
        if Z_IN <= x"0100" then 
            trigflag <= '0';
        end if; 
      end if;
    else 
    sigtest <= -signed(x_out); 
              if trigflag = '0' then 
      Z_IN <= std_logic_vector(signed(Z_IN) +100);
        if Z_IN >= x"2200" then 
            trigflag <= '1';
            flag2 <= '0';

        end if;     
      else 
      Z_IN <= std_logic_vector(signed(Z_IN) -100);
        if Z_IN <= x"0100" then 
            trigflag <= '0';
        end if; 
      end if;
    
    end if;  
    end if;
  end process dut;



end;

