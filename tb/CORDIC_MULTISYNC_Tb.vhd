
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
entity CORDIC_PIPELINE_tb is
end;

architecture bench of CORDIC_PIPELINE_tb is

  component CORDIC_PIPELINE
    generic (
      DEPTH : integer;
      NUMITER : integer
    );
      port (
      I_CLK : in std_logic;
      I_RESET : in std_logic;
      Z_IN : in std_logic_vector(DEPTH-1 downto 0);
      X_OUT : out std_logic_vector(DEPTH-1 downto 0);
      Y_OUT : out std_logic_vector(DEPTH-1 downto 0);
      Z_DEL : out std_logic_vector(DEPTH-1 downto 0)
    );
  end component;

  -- Clock period
  constant clk_period : time := 1.25 us;
  -- Generics
  constant DEPTH : integer := 22;
  constant NUMITER : integer := 13;
  
  -- Ports
  signal I_CLK : std_logic;
  signal I_RESET : std_logic;
  signal Z_IN : std_logic_vector(DEPTH-1 downto 0) := (others => '0');
  signal X_OUT : std_logic_vector(DEPTH-1 downto 0);
  signal Y_OUT : std_logic_vector(DEPTH-1 downto 0);

    signal fsm : std_logic_vector(1 downto 0) := (others => '0');
    signal ctr : unsigned(DEPTH+1 downto 0) := (others => '0');
  -- TRIANGLE (unsigned)
  signal TRI_RESET : std_logic;
  signal Z_TRI : signed(DEPTH-1 downto 0) := (others => '0');
  signal TRI_FLAGS : std_logic_vector(1 downto 0) := (others => '0');
  signal pmtriflags : std_logic_vector(1 downto 0) := (others => '0');
  signal Z_PMTRI : signed(DEPTH-1 downto 0) := (others => '0');
  -- SAW FULL-ROLLOVER(0-2pi)
  signal SAW_RESET : std_logic;
  signal Z_SAW : signed(DEPTH-1 downto 0) := (others => '0');
  
  -- NO-ROLLOVER SAW (0-pi/4) 
  signal NRS_RESET : std_logic;
  signal Z_NRS : signed(DEPTH-1 downto 0) := (others => '0');

  --SAW-RETURN-TO-NEGATIVE (0-pi/2)
  signal RTN_RESET : std_logic;
  signal Z_RTN : signed(DEPTH-1 downto 0) := (others => '0');
  
  --TEST
  signal Z_SIGNED : signed(DEPTH-1 downto 0) := (others => '0');
  signal X_SIGNED : signed(DEPTH-1 downto 0) := (others => '0');
  signal Y_SIGNED : signed(DEPTH-1 downto 0) := (others => '0');
  signal sqx : std_logic := '0';
  signal sqy: std_logic := '0';
  constant step : integer := 2630;

  signal Z_DEL : std_logic_vector(DEPTH-1 downto 0);
  signal Z_DEL_SIG : signed(DEPTH-1 downto 0) := (others => '0');

begin

  CORDIC_PIPELINE_inst : CORDIC_PIPELINE
    generic map (
      DEPTH => DEPTH,
      NUMITER => NUMITER
    )
    port map (
      I_CLK => I_CLK,
      I_RESET => I_RESET,
      Z_IN => std_logic_vector(Z_PMTRI),
      X_OUT => X_OUT,
      Y_OUT => Y_OUT,
      Z_DEL => Z_DEL
    );

  clk_process : process
  begin
  I_CLK <= '1';
  wait for clk_period/2;
  I_CLK <= '0';
  wait for clk_period/2;
  end process clk_process;

  -- purpose: Generate a triangle wave
  -- type   : sequential
  -- inputs : I_CLK, TRI_RESET
  -- outputs: 
--  triangle: process (I_CLK, TRI_RESET) is
--  begin  -- process triangle
--    if TRI_RESET = '0' then             -- asynchronous reset (active low)
--       Z_TRI <= (others => '0');
--    elsif I_CLK'event and I_CLK = '1' then 
--          -- rising clock edge
--       case TRI_FLAGS is
--         when "00" =>
--           Z_TRI <= Z_TRI + step;
--           if Z_TRI = (step*NUMITER) then
--            sqy <= not sqy;
--           end if;
           
--           if Z_TRI(DEPTH-1 downto DEPTH-2) = "01" then
--             TRI_FLAGS <= "01";
           
--           end if;  
           
--         when "01" =>
--           Z_TRI <= Z_TRI -step;
--           if Z_TRI = 2**(DEPTH-2) - (step*NUMITER) - (2**(DEPTH-2) mod step)then
--            sqx <= not sqx;
--           end if;
           
--           if Z_TRI <= step then
--             TRI_FLAGS <= "00";
--           end if;  
--           when others => null;
--         end case;  
--    end if;
--  end process triangle;


pmtriangle : process (I_CLK, TRI_RESET) is 
begin 

    if TRI_RESET = '0' then 
    
    elsif I_CLK'event and I_CLK = '1' then
    if Z_DEL_SIG = (2**(DEPTH-2) - (2**(DEPTH-2) mod step)) + step*2 then
        sqx <= not sqx;
    elsif  Z_DEL_SIG = -((2**(DEPTH-2) - (2**(DEPTH-2) mod step)) + step*2) then 
        sqx <= not sqx;    
    end if;
        case pmtriflags is 
        when "00" => 
            Z_PMTRI <= Z_PMTRI + step;
            if Z_PMTRI(DEPTH-1 downto DEPTH-2) = "01" then 
                pmtriflags <= "01";
            else 
                pmtriflags <= "00";
            end if;     
        when "01" => 
            Z_PMTRI <= Z_PMTRI - step;
            if Z_PMTRI(DEPTH-1 downto DEPTH-2) = "10" then 
              pmtriflags <= "00";
            else
                pmtriflags <= "01";
            end if;
        when others => 
            null;
    end case; 
    end if;    
end process pmtriangle;

--  -- purpose: generate a (0-2pi) saw
--  -- type   : sequential
--  -- inputs : I_CLK, SAW_RESET
--  -- outputs: 
--  sawtooth: process (I_CLK, SAW_RESET) is
--  begin  -- process sawtooth
--    if SAW_RESET = '0' then             -- asynchronous reset (active low)
--      Z_SAW <= (others => '0');
--    elsif I_CLK'event and I_CLK = '1' then  -- rising clock edge
--      Z_SAW <= Z_SAW +step;
--    end if;
--  end process sawtooth;

--  -- purpose: non-rollover sawtooth (0-pi/4)
--  -- type   : sequential
--  -- inputs : I_CLK, NRS_RESET
--  -- outputs: 
--  nonroll: process (I_CLK, NRS_RESET) is
--  begin  -- process nonroll
--    if NRS_RESET = '0' then             -- asynchronous reset (active low)
--      Z_NRS <= (others => '0');
--    elsif I_CLK'event and I_CLK = '1' then  -- rising clock edge
--      Z_NRS <= Z_NRS +step;
--      if Z_NRS(DEPTH-1 downto DEPTH-2) = "01" then
--        Z_NRS <= (others => '0');
--      end if;
--    end if;
--  end process nonroll;

--    rettonegative: process (I_CLK, RTN_RESET) is
--  begin  -- process rettonegative
--    if RTN_RESET = '0' then             -- asynchronous reset (active low)
--      Z_RTN <= (others => '0');
--    elsif I_CLK'event and I_CLK = '1' then  -- rising clock edge
--      Z_RTN <= Z_RTN +step;
--      if Z_RTN(DEPTH-1 downto DEPTH-2) = "01" then
--        Z_RTN <= -Z_RTN;
--    end if;
--    end if;
--  end process rettonegative;

   
   
   
        Z_SIGNED <= signed(Z_IN);
        X_SIGNED <= signed(X_OUT) when sqx = '0' else -signed(X_OUT) ;
        Y_SIGNED <= signed( Y_OUT);
        Z_DEL_SIG <= signed(Z_DEL);

end;
