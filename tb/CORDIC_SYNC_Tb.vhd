library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use STD.textio.all;
use ieee.std_logic_textio.all;


entity CORDIC_SYNC_Tb is
  
end entity CORDIC_SYNC_Tb;

architecture testbench of CORDIC_SYNC_Tb is
  
  component CORDIC_SYNC is
    generic (
      DEPTH   : integer;
      NUMITER : integer;
      step    : integer);
    port (
      I_CLK   : in  std_logic;
      I_RST_N : in  std_logic;
      COS_O   : out signed(DEPTH-1 downto 0);
      SIN_O   : out signed(DEPTH-1 downto 0));
  end component CORDIC_SYNC;

  constant DEPTH   : integer := 22;
  constant NUMITER : integer := 18;
  constant step    : integer := 2630;

  signal I_CLK   : std_logic;
  signal I_RST_N : std_logic;
  signal COS_O_1   : signed(DEPTH-1 downto 0);
  signal SIN_O_1  : signed(DEPTH-1 downto 0);

    signal COS_O_2   : signed(DEPTH-1 downto 0);
  signal SIN_O_2  : signed(DEPTH-1 downto 0);
 signal mullout : signed(2*DEPTH-1 downto 0); 
 signal nullout : signed(2*DEPTH-1 downto 0);
 signal ruleout : signed(2*DEPTH-21 downto 0);
  constant clk_period : time := 1.25us;
  constant rate : integer := 512;
  constant altclk_per : time := rate*clk_period;                    
 signal I_ALTCLK : std_logic := '1';
  file file_VECTORS : text;
  signal idata : integer;
  signal sdata : signed(DEPTH-1 downto 0);
begin  -- architecture testbench

  CORDIC_SYNC_1: CORDIC_SYNC
    generic map (
      DEPTH   => DEPTH,
      NUMITER => NUMITER,
      step    => step)
    port map (
      I_CLK   => I_CLK,
      I_RST_N => I_RST_N,
      COS_O   => COS_O_1,
      SIN_O   => SIN_O_1);
  
    CORDIC_SYNC_2: CORDIC_SYNC
    generic map (
      DEPTH   => DEPTH,
      NUMITER => NUMITER,
      step    => step/2)
    port map (
      I_CLK   => I_CLK,
      I_RST_N => I_RST_N,
      COS_O   => COS_O_2,
      SIN_O   => SIN_O_2);

  clk_process : process
  begin
  I_CLK <= '1';
  wait for clk_period/2;
  I_CLK <= '0';
  wait for clk_period/2;
  end process clk_process;
  
altclkgen : process
begin
I_ALTCLK <= not I_ALTCLK;
wait for clk_period/2;
I_ALTCLK <= not I_ALTCLK;
wait for clk_period/2;
end process altclkgen;


 I_RST_N <= '1';

  mult : process (I_CLK, I_RST_N)
  begin
    if I_RST_N = '0' then 
    elsif (rising_edge(I_CLK)) then 
        mullout <= SIN_O_1 * SIN_O_2;
        nullout <= shift_right(mullout, 20);
        ruleout <= resize(nullout, ruleout'length);
    end if;
  
  end process mult;

  
  reading: process 
    variable v_input: line; 
    variable v_data: integer; 
   
  begin
  file_open(file_VECTORS, "data.txt",  read_mode);
    while not endfile(file_VECTORS) loop
      readline(file_VECTORS, v_input);
      read(v_input, v_data);
      idata <= v_data;
      sdata <= TO_SIGNED(idata, DEPTH);
      wait for 1.25us;
    end loop;
  
  end process;
  
  
end architecture testbench;
